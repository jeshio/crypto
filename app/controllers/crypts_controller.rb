class CryptsController < ApplicationController
	# Шифр Цезаря
	# arg - количество сдвигов
	# non_sym - массив пропускаемых символов
	def encode_caeser(text, arg, non_sym)
		return text.split(//).map { |c| !non_sym.include?(c) ? (c.ord + arg).chr : c }.join()
	end
	def decode_caeser(text, arg, non_sym)
		return text.split(//).map { |c| !non_sym.include?(c) ? (c.ord - arg).chr : c }.join()
	end

	def index
		@non_sym = " ,;:.!?'\"-()[]{}"
	end

	def make_crypt
		@text = params[:crypt][:text]
		@crypt_id = params[:crypt][:crypt_id]
		@arg = params[:crypt][:arg]
		@non_sym = params[:crypt][:non_sym]
		@non_sym_arg = @non_sym.split(//)
		@encode = params[:crypt][:encode] == "1" ? true : false

		case @crypt_id
		when "1"
			@result = @encode ? encode_caeser(@text, @arg.to_i, @non_sym_arg) : decode_caeser(@text, @arg.to_i, @non_sym_arg)
		end

		respond_to do |format| 
			format.js
		end
	end
end
